import React from "react";
import Background from "../assest/images/background-food-header.png";
import HeaderImage1 from "../assest/images/Rectanglephone-image-food-app.png";
import HeaderImage2 from "../assest/images/Rectanglephone-image-food-app2.png";
import WorkImage1 from "../assest/images/feature-work-image1.png";
import WorkImage2 from "../assest/images/feature-work-image2.png";
import WorkImagePay from "../assest/images/feature-work-image-pay.png";
import styled from 'styled-components';

class Home extends React.Component {
  render() {
    const Flexitem = styled.div `
    display: flex;
    justify-content: center;`
    return (
      <div className=" wrapper">
        <div
          className="header">
          <div className="container">
            <div className="row header__text">
              <div className="col-md-8">
                <h6> Food app </h6>
                <h1> Why stay hungry when you can order form Bella Onojie </h1>
                <h4> Download the bella onoje’ s food app now on </h4>
                <div className="row btn__wrap">
                  <div className="col-md-4">
                    <button className="btn playstore"> Playstore </button>
                  </div>
                  <div className="col-md-4">
                    <button className="btn appstore"> App store </button>
                  </div>
                </div>
              </div>
            </div>
            <div className="wrap__image container ">
              <div className="row image__header">
                <div className="col-md-4 col-sm-2 image__header--food">
                  <img
                    className=" image__header--food1"
                    src={HeaderImage1}
                    alt=""
                  />
                </div>
                <div className="col-md-4 col-sm-2 image__header--food">
                  <img
                    className=" image__header--food2"
                    src={HeaderImage2}
                    alt=""
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="content">
          <div className="container">
            <div className="row header__detail">
              <h1> How the app works </h1>
            </div>
          </div>
          <div className="app__work">
            <div className="container">
              <div className="row app__work--flow">
                <div className="col-md-6  app__work--image">
                  <img src={WorkImage1} alt="" />
                </div>
                <div className="col-md-6 app__work--text">
                  <div className="app__work--text--detail">
                    <h3> Create an account </h3>
                    <h2>
                      Create / login to an existing account to get started
                    </h2>
                    <h4>
                      An account is created with your email and a desired
                      password
                    </h4>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="app__shop">
            <div className="container">
              <div className="row">
                <div className="col-md-6 app__work--text">
                  <div className="app__work--text--detail">
                    <h3> Explore varieties </h3>
                    <h2> Shop for your favorites meal as e dey hot. </h2>
                    <h4>
                      
                      Shop for your favorite meals or drinks and enjoy while
                      doing it.
                    </h4>
                  </div>
                </div>
                <div className="col-md-6  app__work--image">
                  <img src={WorkImage2} alt="" />
                </div>
              </div>
            </div>
          </div>
          <div className="app__checkout">
            <div className="container">
              <div className="row app__checkout--flow">
                <div className="col-md-6  app__work--image">
                  <img src={WorkImagePay} alt="" />
                </div>
                <div className="col-md-6 app__work--text">
                  <div className="app__work--text--detail">
                    <h3> Checkout </h3>
                    <h2> When you done check out and get it delivered. </h2>
                    <h4>
                      When you done check out and get it delivered with ease.
                    </h4>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="app__download">
            <Flexitem
              className="container ">
              <div className="row app__download--contain">
                <div className="col-md-10 app__download--text">
                  <h2> Download the app now. </h2>
                  <h4>
                 
                    Available on your favorite store.Start your premium
                    experience now
                  </h4>
                  <div className="row btn__wrap">
                    <div className="col-md-3">
                      <button className="btn btn__playstore">
                        playstore
                      </button>
                    </div>
                    <div className="col-md-3">
                      <button className="btn btn__appstore"> App store </button>
                    </div>
                  </div>
                </div>
              </div>
            </Flexitem>
          </div>
        </div>
      </div>
    );
  }
}
export default Home;
