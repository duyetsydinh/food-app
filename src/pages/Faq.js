import React, { Component } from "react";

class Faq extends Component {
  
  render() {
    return (
      <div className="container">
       <div className="row">
       <div className="col-md-12">
        <video
          autoplay="true"
          style={{
            WebkitMaskBoxImage:
              'url("https://media.flaticon.com/dist/min/img/video/rex/mask.svg")',
            maskImage:
              'url("https://media.flaticon.com/dist/min/img/video/rex/mask.svg")',
          }}
        >
          <source
            src="https://media.flaticon.com/dist/min/img/video/rex/video.mp4"
            type="video/mp4"
          />
        </video>
      </div>
      </div>
      </div>
    );
  }
}
export default Faq;
