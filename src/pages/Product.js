import React, { Component } from 'react'
import ProductCss from '../styles/Product.scss'
import Model from '../components/Modal'
import styled from 'styled-components'
class Product extends Component {
  constructor (props) {
    super(props)

    this.state = {
      list: [
        {
          title: 'chicken',
          image:
            'https://chickenplus.com.vn/resize/380x260/1/upload/baiviet/9941213629529567814916825141932183891476480n-6944.jpg',
          price: '100k',
          id: 0
        },
        {
          title: 'vegetable',
          image:
            'https://vinmec-prod.s3.amazonaws.com/images/20190425_063710_470758_rau_xanh.max-800x800.jpg',
          price: '100k',
          id: 1
        },
        {
          title: 'fishing',
          image:
            'https://daotaobeptruong.vn/wp-content/uploads/2016/08/ca-kho-to-thom-ngon.jpg',
          price: '100k',
          id: 2
        },
        {
          title: 'rice',
          image:
            'https://viorice.com/wp-content/uploads/2019/04/dai-ly-cung-cap-gao-tam-gia-re-cho-dai-ly-va-quan-com-tai-tphcm.jpg',
          price: '100k',
          id: 3
        }
      ],

      list2: [
        {
          title: 'Beef',
          image:
            'https://cdn-www.vinid.net/2020/03/mua-b%C3%B2-m%E1%BB%B9-%E1%BB%9F-%C4%91%C3%A2u-ngon-nh%E1%BA%A5t-1.jpg',
          price: '200k',
          id: 4
        },
        {
          title: 'Bun bo',
          image: 'https://toplist.vn/images/800px/bun-bo-ba-dieu-534581.jpg',
          price: '70k',
          id: 5
        },
        {
          title: 'fishing',
          image:
            'https://afamilycdn.com/150157425591193600/2020/8/6/11711435017206607747416694430819408613724189o-15966988770602088451375.jpg',
          price: '50k',
          id: 6
        },
        {
          title: 'rice',
          image:
            'https://viorice.com/wp-content/uploads/2019/04/dai-ly-cung-cap-gao-tam-gia-re-cho-dai-ly-va-quan-com-tai-tphcm.jpg',
          price: '200k',
          id: 7
        }
      ],

      list3: [
        {
          title: 'Beef',
          image:
            'https://cdn-www.vinid.net/2020/03/mua-b%C3%B2-m%E1%BB%B9-%E1%BB%9F-%C4%91%C3%A2u-ngon-nh%E1%BA%A5t-1.jpg',
          price: '200k',
          id: 8
        },
        {
          title: 'Bun bo',
          image: 'https://toplist.vn/images/800px/bun-bo-ba-dieu-534581.jpg',
          price: '70k',
          id: 9
        },
        {
          title: 'fishing',
          image:
            'https://afamilycdn.com/150157425591193600/2020/8/6/11711435017206607747416694430819408613724189o-15966988770602088451375.jpg',
          price: '50k',
          id: 10
        },
        {
          title: 'rice',
          image:
            'https://viorice.com/wp-content/uploads/2019/04/dai-ly-cung-cap-gao-tam-gia-re-cho-dai-ly-va-quan-com-tai-tphcm.jpg',
          price: '200k',
          id: 11
        }
      ]
    }
  }

  render () {
    const Banner = styled.div`
      justify-content: center;
      padding-top: 40px;
      padding-bottom: 40px;
      text-align: center;
    `
    const ImageBanner = styled.img`
      border-radius: 20px;
      width: 100%;
      height: 350px;
      object-fit: cover;
    `
    const PaddingDiv = styled.div`
      padding-top: 30px;
    `
    const PaddingElement = styled.h1`
      padding: 20px 0px;
    `

    return (
      <div className='container'>
        <Banner className='banner'>
          <ImageBanner
            src='https://cdn.dribbble.com/users/3320958/screenshots/14796305/media/f1033ec67c3d8781eb80b9547b9a8f85.jpeg?compress=1&resize=800x600'
            alt='banner'
          />
        </Banner>
        <PaddingElement>List product</PaddingElement>
        <div className='list__product'>
          <div className='container'>
            <div className=' row '>
              {this.state.list3.map((element, index) => {
                return (
                  <div className='col-md-2 list__product--item' key={index}>
                    <img src={element.image} alt='' />
                    <h2>{element.title}</h2>
                    <p>{element.price} </p>
                  </div>
                )
              })}{' '}
            </div>
            <PaddingDiv className=' row '>
              {this.state.list.map((element, index) => {
                return (
                  <div className='col-md-3 list__product--item' key={index}>
                    <a href={element.image}>
                      <img src={element.image} />
                    </a>
                    <h2>{element.title}</h2>
                    <p>{element.price} </p>
                    <Model
                      propsValue={element.id}
                      image={element.image}
                      title={element.title}
                      price={element.price}
                    ></Model>
                  </div>
                )
              })}{' '}
            </PaddingDiv>
            <PaddingDiv className=' row '>
              {this.state.list2.map((element, index) => {
                return (
                  <div className='col-md-3 list__product--item' key={index}>
                    <img src={element.image} alt='' />
                    <h2>{element.title}</h2>
                    <p>{element.price} </p>
                    <Model
                      propsValue={element.id}
                      image={element.image}
                      title={element.title}
                      price={element.price}
                    ></Model>
                  </div>
                )
              })}{' '}
            </PaddingDiv>
          </div>
        </div>
      </div>
    )
  }
}

export default Product
