
import React from 'react';

function Model(props) {
  let link = 'https://5fc3323b9210060016869f8a.mockapi.io/api/flower-v1/datas';

  const add = () => {
    async function postData(url = link, data = {}) {

      const response = await fetch(url, {
        method: 'POST',
        mode: 'cors',
        cache: 'no-cache',
        credentials: 'same-origin',
        headers: {
          'Content-Type': 'application/json'
        },
        redirect: 'follow',
        referrerPolicy: 'no-referrer',
        body: JSON.stringify(data)
      });
      return response.json();
    }

    postData(link,
      { id: props.propsValue, image: props.image, title: props.title, price: props.price })
      .then(data => {
        console.log(data);
        document.querySelector(".data__image").src = data.image;
        document.querySelector(".data__title").innerHTML = data.title;
        document.querySelector(".data__price").innerHTML = data.price;
      })
  }

  return (
    <div>
      <button className="btn btn__item" onClick={add}
        data-toggle="modal" data-target="#exampleModal">Buy</button>
      <div className="modal fade" id="exampleModal" tabIndex={-1}
        role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title" id="exampleModalLabel">Menu Today</h5>
              <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
              </button>
            </div>
            <div className="modal-body">

              <h1 className="data__title"></h1>
              <img className="data__image" alt="" />
              <p className="data__price"></p>

            </div>
            <div className="modal-footer">
              <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="button" className="btn btn-primary">Buy Now</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Model;