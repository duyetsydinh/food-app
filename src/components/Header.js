import React, { Component } from 'react';
import { Link } from "react-router-dom";
import Logo from '../assest/images/logo-food.png';

class Header extends Component {

  render() {
    return (

      <div className="container">
        <nav className="navbar navbar-expand-lg navbar-light feature" >
          <a className="navbar-logo " href="#">
            <img className="logo " id="main-logo" src={Logo} alt="" />
          </a>
          <button className="navbar-toggler" type="button"
            data-toggle="collapse" data-target="#navbarNavAltMarkup"
            aria-controls="navbarNavAltMarkup"
            aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon" ></span>

          </button>
          <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
            <div className="navbar-nav ">

              <li className="nav-link"> <Link to="/">Home </Link></li>
              <li className="nav-link"><Link to="/Product">Product</Link> </li>
              <li className="nav-link"><Link to="/Faq">Faq</Link> </li>
              <li className="nav-link"><Link to="/Contact">Contact</Link> </li>
            </div>
          </div>
        </nav>

      </div>

    )
  }
}

export default Header;