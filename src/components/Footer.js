import React, { Component } from 'react';
import Logo from '../assest/images/logo-food.png';
import IconTwiter from '../assest/images/icons/ant-design_twitter-outlinedtwiter-icon.png';
import IconFaceBook from '../assest/images/icons/ant-design_facebook-filledfacebook-icon.png';
import IconCamera from '../assest/images/icons/instagramicon-instagram.png';
import FooterCss from '../styles/Footer.scss'

class Footer extends Component {
  render() {
    return (

      <div className="app__footer">
        <div className="container">
          <div className="row">
            <div className="col-md-4">
              <div className="logo">
                <a href="#"><img src={Logo}
                  alt="" /></a>
              </div>
            </div>
            <div className="col-md-4">
              <div className="social__icon">
                <a href="#"><img src={IconTwiter}
                  alt="" /></a>
                <a href="https://www.facebook.com/xautrai.duyet.3">
                  <img src={IconFaceBook}
                    alt="" /></a>
                <a href="#"><img src={IconCamera}
                  alt="" /></a>
              </div>
            </div>
            <div className="col-md-4">
              <div className="copywrite">
                <p>Copywright 2021 Dinh Si Duyet</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }

}
export default Footer;
