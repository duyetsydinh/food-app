import React from 'react';
import { Switch, Route } from 'react-router-dom';

import Home from './pages/Home';
import Contact from './pages/Contact';
import About from './pages/About';
import Faq from './pages/Faq';
import Product from './pages/Product';
export default function Routes() {
  return (
    <Switch>
      <Route path="/" exact component={Home} />
      <Route path="/Contact" component={Contact} />
      <Route path="/About" component={About}/>
      <Route path="/Faq" component={Faq}/>
      <Route path="/Product" component={Product}/>
    
    </Switch>
  );
}