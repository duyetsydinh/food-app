import React from 'react';
import { BrowserRouter as Router } from "react-router-dom";
import Routes from './Routes';
import Header from './components/Header';
import './styles/App.scss'
import Footer from './components/Footer';
import Sticky from 'react-sticky-el';

class App extends React.Component {
  render() {
    return (
      <Router>
        <Sticky>
          <Header />
        </Sticky>

        <div className="content">
          <Routes />
        </div>

        <Footer />
      </Router>
    )
  }
}

export default App;
